<?php

function substract(array $nums)
{
    return array(max($nums));
}

function divided(array $nums, int $x)
{
    $result = [];
    foreach ($nums as $num) {
        foreach ($nums as $number) {
            $divide = $num / $number;
            if ($divide == $x) {
                $result[] = $num;
            }
        }
    }
    return array_diff($nums, $result);
}

function arrayStrings(string $word, int $x)
{
    $splices = explode(" ", $word, $x);
    $result = [];
    foreach ($splices as $splice) {
        if (strlen($splice) == $x) {
            $result[] = $splice;
        }
    }
    return $result;
}

print_r(substract([1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5]));
echo "<br>";
print_r(divided([1, 2, 3, 4, 5, 6], 3));
echo "<br>";
print_r(arrayStrings("souvenir loud four lost", 4));